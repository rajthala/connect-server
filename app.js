var express = require('express');
var bodyParser = require('body-parser');
const fs = require('fs');
var path = require('path');

var jsonfile = require('./ninja.json');
var app = express();
var jsonData=null;

// var http = require("http");
// var options = {
//   hostname: 'localhost',
//   port: 8080,
//   path: '/login',
//   method: 'POST',
//   headers: {
//       'Content-Type': 'application/json',
//   }
// };
// var req = http.request(options, function(res) {
//   console.log('Status: ' + res.statusCode);
//   console.log('Headers: ' + JSON.stringify(res.headers));
//   res.setEncoding('utf8');
//   res.on('data', function (body) {
//     console.log('Body: ' + body);
//   });
// });
// req.on('error', function(e) {
//   console.log('problem with request: ' + e.message);
// });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set('view engine', 'ejs');

app.get('/', function(req, res) {
    res.render('index');
});


var usersFilePath = path.join(__dirname, 'ninja.json');

// app.get('/login', function(req, res){
//     var readable = fs.createReadStream(usersFilePath);
//     readable.pipe(res);
// });


app.get('/login', function(req, res) {

    jsonData = JSON.stringify({
        name: req.param('username'),
        password: req.param('password')
    });
    var json = JSON.parse(jsonData);
    res.status(200);
    console.log(jsonData);
    res.json(jsonData);
    // var readable = fs.createReadStream(usersFilePath);
    //     readable.pipe(res);
});

// app.post('/login', function (req, res) {
//     jsonData = JSON.stringify(req.body);
//     res.status(200).send(jsonData);
//     console.log(jsonData);
// });


app.listen(3030);
console.log("listening to port 3030........");
